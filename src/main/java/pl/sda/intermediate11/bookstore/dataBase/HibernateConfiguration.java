package pl.sda.intermediate11.bookstore.dataBase;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateConfiguration {

    private static EntityManagerFactory factory;

    static { // blok  statyczny wywolany jest tylko razy tylko przy tworzeniu obiektu na smym poczatku
        factory = Persistence.createEntityManagerFactory("bookStoreDataBase");
    }

    public static EntityManager getEntityMenager(){
        return factory.createEntityManager();
    }

    public static void close(){
        factory.close();
    }
}
